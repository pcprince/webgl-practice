/****************************************************************************
 * index.js
 * openacousticdevices.info
 * July 2021
 *****************************************************************************/

const drawingCanvas = document.getElementById('drawing-canvas');

// TODO: Use gl_VertexIndex to calculate x and y when a colour is given

const vertexShaderText =
[
    'precision mediump float;',
    '',
    'attribute vec2 vertPosition;',
    'attribute vec3 vertColor;',
    'varying vec3 fragColor;',
    '',
    'void main()',
    '{',
    '   gl_PointSize = 1.0;',
    '   fragColor = vertColor;',
    '   gl_Position = vec4(vertPosition, 0.0, 1.0);',
    '}'
].join('\n');

const fragmentShaderText =
[
    'precision mediump float;',
    '',
    'varying vec3 fragColor;',
    '',
    'void main ()',
    '{',
    '   gl_FragColor = vec4(fragColor, 1.0);',
    '}'
].join('\n');

const initDemo = function () {

    console.log('Loaded');

    /** @type {WebGLRenderingContext} */
    let gl = drawingCanvas.getContext('webgl');

    if (!gl) {

        console.log('Loading experimental WebGL context');
        gl = drawingCanvas.getContext('experimental-webgl');

    }

    if (!gl) {

        console.error('WebGL not supported by this browser');
        return;

    }

    // gl.clearColor(0.75, 0.85, 0.8, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    const vertexShader = gl.createShader(gl.VERTEX_SHADER);
    const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(vertexShader, vertexShaderText);
    gl.shaderSource(fragmentShader, fragmentShaderText);

    gl.compileShader(vertexShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {

        console.error('Error compiling vertex shader', gl.getShaderInfoLog(vertexShader));
        return;

    }

    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {

        console.error('Error compiling fragment shader', gl.getShaderInfoLog(fragmentShader));
        return;

    }

    const program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);

    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {

        console.error('Error linking program', gl.getProgramInfoLog(program));
        return;

    }

    // TODO: Remove this in release
    gl.validateProgram(program);
    if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {

        console.error('Error validating program', gl.getProgramInfoLog(program));

    }

    // Create buffer

    const pointData = [];

    for (let i = 0; i < 500; i++) {

        for (let j = 0; j < 200; j++) {

            const x = (i / drawingCanvas.width * 2.0) - 1.0;
            const y = (j / drawingCanvas.height * 2.0) - 1.0;

            pointData.push(x, y, 0.5, 0.0, 0.0);

        }

    }

    const pointVertexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, pointVertexBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(pointData), gl.STATIC_DRAW);

    const positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');
    const colorAttribLocation = gl.getAttribLocation(program, 'vertColor');

    gl.vertexAttribPointer(
        positionAttribLocation, // Attribute location
        2, // Number of elements per attribute
        gl.FLOAT, // Type of elements
        gl.FALSE,
        5 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
        0 // Offset from the bgeinning of a single vertex to this attribute
    );

    gl.vertexAttribPointer(
        colorAttribLocation, // Attribute location
        3, // Number of elements per attribute
        gl.FLOAT, // Type of elements
        gl.FALSE,
        5 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
        2 * Float32Array.BYTES_PER_ELEMENT // Offset from the bgeinning of a single vertex to this attribute
    );

    gl.enableVertexAttribArray(positionAttribLocation);
    gl.enableVertexAttribArray(colorAttribLocation);

    // Main render loop

    gl.useProgram(program);
    gl.drawArrays(gl.POINTS, 0, pointData.length / 5);

};
